<%--
  Created by IntelliJ IDEA.
  User: tnguyen
  Date: 12/04/2020
  Time: 10:18
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Visualisation of Organisms from BioModels</title>
    <script type="text/javascript" src="https://d3js.org/d3.v4.min.js"></script>
</head>

<body>
<div id="organismsChart" class="div-center-content"></div>
<g:javascript>
    var dataset = JSON.parse("${organisms}");
    console.log(dataset);
    var width = 600, height = 600, diameter = 600;
    var color = d3.scaleOrdinal(d3.schemeCategory20);
    var bubble = d3.pack(dataset).size([diameter, diameter]).padding(1.5);

    var svg = d3.select("#organismsChart")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "bubble");

    var nodes = d3.hierarchy(dataset)
        .sum(function(d) {
            return d.count; });

    var node = svg.selectAll(".node")
        .data(bubble(nodes).descendants())
        .enter()
        .filter(function(d){
        return  !d.children
        })
        .append("g")
        .attr("class", "node")
        .attr("transform", function(d) {
            console.log(d);
        return "translate(" + d.x + "," + d.y + ")";
    });

    node.append("title")
        .text(function(d) {
            return d.data.name + ": " + d.data.count;
    });

    node.append("circle")
        .attr("r", function(d) {
        return d.r;
    })
        .style("fill", function(d,i) {
        return color(i);
    });

    node.append("text")
        .attr("dy", ".2em")
        .style("text-anchor", "middle")
        .text(function(d) {return d.data.name.substring(0, d.r / 3);})
        .attr("font-family", "sans-serif")
        .attr("font-size", function(d) {return d.r/5;})
        .attr("fill", "white");

    node.append("text")
        .attr("dy", "1.3em")
        .style("text-anchor", "middle")
        .text(function(d) { return d.data.count;})
        .attr("font-family",  "Gill Sans", "Gill Sans MT")
        .attr("font-size", function(d) { return d.r/5;})
        .attr("fill", "white");

    d3.select(self.frameElement)
        .style("height", diameter + "px");
    node.on('click', function(d) {
        console.log(d);
        var label = d["data"]["name"];
        var value = d["data"]["count"];
        var taxonomy = d["data"]["taxonomy"];
        var serverURL = "https://www.ebi.ac.uk/biomodels";
        var fixedSearchURL = "/search?domain=biomodels&query=*%3A*+AND+TAXONOMY%3A";
        var prefixSearchURL = serverURL + fixedSearchURL;
        var queryURL = prefixSearchURL + taxonomy;
        window.open(queryURL, '_blank');
    });

</g:javascript>
</body>
</html>
