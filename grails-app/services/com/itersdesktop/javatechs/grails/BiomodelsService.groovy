package com.itersdesktop.javatechs.grails

import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional
class BiomodelsService {

    private def toJsonFromString(final String jsonString) {
        def slurper = new JsonSlurper()
        slurper.parseText(jsonString)
    }

    private Map buildOrganisms(final String jsonString) {
        def parsedJson = toJsonFromString(jsonString)
        def totalTermCount = parsedJson.totalTermCount as long
        def jsonOrganisms = parsedJson.topTerms
        List topTerms = new ArrayList<Organism>()
        for (def entry : jsonOrganisms) {
            String taxonomy = entry.text
            long count = entry.docFreq
            String name = resolveTaxonomyTerm(taxonomy)
            topTerms.add(new Organism(name: name, taxonomy: taxonomy, count: count))
        }
        return [totalTermCount: totalTermCount, topTerms: topTerms]
    }

    private String resolveTaxonomyTerm(String s) {
        String serviceURL = "https://www.ebi.ac.uk/ebisearch/ws/rest/taxonomy/entry"
        serviceURL = "${serviceURL}/$s?fields=name"
        RestBuilder rest = new RestBuilder(connectTimeout: 10000, readTimeout: 100000, proxy: null)
        def response = rest.get(serviceURL) {
            accept("application/xml")
            contentType("application/xml;charset=UTF-8")
        }
        if (response.status == 200) {
            return response.xml
        }
        return null
    }

    def build() {
        def jsonString = '''
{"totalTermCount":1745,"topTerms":[{"text":"9606","docFreq":713},{"text":"4932","docFreq":137},{"text":"10090","docFreq":114},{"text":"40674","docFreq":95},{"text":"131567","docFreq":77},{"text":"562","docFreq":45},{"text":"10114","docFreq":36},{"text":"2759","docFreq":30},{"text":"8355","docFreq":27},{"text":"10116","docFreq":24},{"text":"3702","docFreq":22},{"text":"9986","docFreq":17},{"text":"33090","docFreq":14},{"text":"3701","docFreq":12},{"text":"10141","docFreq":12},{"text":"9615","docFreq":11},{"text":"7711","docFreq":11},{"text":"7227","docFreq":11},{"text":"5691","docFreq":10},{"text":"39107","docFreq":10},{"text":"5141","docFreq":7},{"text":"4896","docFreq":7},{"text":"9913","docFreq":5},{"text":"83333","docFreq":5},{"text":"33208","docFreq":5},{"text":"2242","docFreq":5},{"text":"1773","docFreq":5},{"text":"10088","docFreq":5},{"text":"8782","docFreq":4},{"text":"8292","docFreq":4},{"text":"7742","docFreq":4},{"text":"7215","docFreq":4},{"text":"5658","docFreq":4},{"text":"559292","docFreq":4},{"text":"33154","docFreq":4},{"text":"3193","docFreq":4},{"text":"2208","docFreq":4},{"text":"210","docFreq":4},{"text":"1423","docFreq":4},{"text":"1358","docFreq":4},{"text":"9685","docFreq":3},{"text":"5833","docFreq":3},{"text":"4952","docFreq":3},{"text":"4922","docFreq":3},{"text":"2","docFreq":3},{"text":"11676","docFreq":3},{"text":"11320","docFreq":3},{"text":"10117","docFreq":3},{"text":"10095","docFreq":3},{"text":"9984","docFreq":2},{"text":"9940","docFreq":2},{"text":"9796","docFreq":2},{"text":"9544","docFreq":2},{"text":"7787","docFreq":2},{"text":"746128","docFreq":2},{"text":"70448","docFreq":2},{"text":"6678","docFreq":2},{"text":"6618","docFreq":2},{"text":"64495","docFreq":2},{"text":"6035","docFreq":2},{"text":"5518","docFreq":2},{"text":"5507","docFreq":2},{"text":"5501","docFreq":2},{"text":"5482","docFreq":2},{"text":"5478","docFreq":2},{"text":"5476","docFreq":2},{"text":"5346","docFreq":2},{"text":"5306","docFreq":2},{"text":"5297","docFreq":2},{"text":"5270","docFreq":2},{"text":"5207","docFreq":2},{"text":"520","docFreq":2},{"text":"5180","docFreq":2},{"text":"51453","docFreq":2},{"text":"5062","docFreq":2},{"text":"5061","docFreq":2},{"text":"5057","docFreq":2},{"text":"5037","docFreq":2},{"text":"4959","docFreq":2},{"text":"4924","docFreq":2},{"text":"4897","docFreq":2},{"text":"4837","docFreq":2},{"text":"40563","docFreq":2},{"text":"40559","docFreq":2},{"text":"38033","docFreq":2},{"text":"3704","docFreq":2},{"text":"36914","docFreq":2},{"text":"36911","docFreq":2},{"text":"36630","docFreq":2},{"text":"34099","docFreq":2},{"text":"336810","docFreq":2},{"text":"33188","docFreq":2},{"text":"33178","docFreq":2},{"text":"329376","docFreq":2},{"text":"3055","docFreq":2},{"text":"29883","docFreq":2},{"text":"28985","docFreq":2},{"text":"262014","docFreq":2},{"text":"197043","docFreq":2},{"text":"186490","docFreq":2},{"text":"155892","docFreq":2},{"text":"1496","docFreq":2},{"text":"148305","docFreq":2},{"text":"140110","docFreq":2},{"text":"13684","docFreq":2},{"text":"117187","docFreq":2},{"text":"109871","docFreq":2},{"text":"1063","docFreq":2},{"text":"1047171","docFreq":2},{"text":"104341","docFreq":2},{"text":"9989","docFreq":1},{"text":"9823","docFreq":1},{"text":"9598","docFreq":1},{"text":"9541","docFreq":1},{"text":"9031","docFreq":1},{"text":"85569","docFreq":1},{"text":"83332","docFreq":1},{"text":"7955","docFreq":1},{"text":"7668","docFreq":1},{"text":"7108","docFreq":1},{"text":"70699","docFreq":1},{"text":"7029","docFreq":1},{"text":"672","docFreq":1},{"text":"6499","docFreq":1},{"text":"644223","docFreq":1},{"text":"63577","docFreq":1},{"text":"629395","docFreq":1},{"text":"61434","docFreq":1},{"text":"58853","docFreq":1},{"text":"5850","docFreq":1},{"text":"5791","docFreq":1},{"text":"5782","docFreq":1},{"text":"5548","docFreq":1},{"text":"5544","docFreq":1},{"text":"53533","docFreq":1},{"text":"511145","docFreq":1},{"text":"5076","docFreq":1},{"text":"49990","docFreq":1},{"text":"4930","docFreq":1},{"text":"4929","docFreq":1},{"text":"4894","docFreq":1},{"text":"4892","docFreq":1},{"text":"4547","docFreq":1},{"text":"452646","docFreq":1},{"text":"44689","docFreq":1},{"text":"416870","docFreq":1},{"text":"4113","docFreq":1},{"text":"4097","docFreq":1},{"text":"402676","docFreq":1},{"text":"39782","docFreq":1},{"text":"3847","docFreq":1},{"text":"381512","docFreq":1},{"text":"36420","docFreq":1},{"text":"33169","docFreq":1},{"text":"32524","docFreq":1},{"text":"317","docFreq":1},{"text":"314146","docFreq":1},{"text":"29875","docFreq":1},{"text":"294746","docFreq":1},{"text":"272634","docFreq":1},{"text":"272563","docFreq":1},{"text":"267377","docFreq":1},{"text":"227321","docFreq":1},{"text":"2257","docFreq":1},{"text":"215813","docFreq":1},{"text":"211044","docFreq":1},{"text":"2104","docFreq":1},{"text":"2099","docFreq":1},{"text":"197911","docFreq":1},{"text":"1758","docFreq":1},{"text":"173629","docFreq":1},{"text":"162425","docFreq":1},{"text":"1590","docFreq":1},{"text":"1472294","docFreq":1},{"text":"132504","docFreq":1},{"text":"11292","docFreq":1},{"text":"11276","docFreq":1},{"text":"11103","docFreq":1},{"text":"1034331","docFreq":1},{"text":"10239","docFreq":1},{"text":"10160","docFreq":1},{"text":"101201","docFreq":1},{"text":"10036","docFreq":1},{"text":"10029","docFreq":1},{"text":"10026","docFreq":1},{"text":"1","docFreq":1}]}
'''
        def result = buildOrganisms(jsonString)
        def organismsMap = result["topTerms"].collect { entry ->
            [name: "${entry.name} (${entry.taxonomy})" as String,
             count: entry.count as Integer,
             taxonomy: entry.taxonomy as String]
        }
        organismsMap
    }

    def fetchAllOrganisms() {
        final String BM_SEARCH_URL = "https://wwwdev.ebi.ac.uk/ebisearch/ws/rest/biomodels/topterms"
        String queryURL = "${BM_SEARCH_URL}/TAXONOMY?facetfield:label&size=500"
        RestBuilder rest = new RestBuilder(connectTimeout: 10000, readTimeout: 100000, proxy: null)
        def response = rest.get(queryURL) {
            accept("application/xml")
            contentType("application/xml;charset=UTF-8")
        }
        if (response.status == 200) {
            return response.xml
        }
        return null
    }
}
