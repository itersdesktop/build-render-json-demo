package com.itersdesktop.javatechs.grails

import grails.converters.JSON

class BiomodelsController {
    def biomodelsService

    /**
     * Builds a JSON representing organisms statistics
     *
     * @return
     */
    def organism() {
        render biomodelsService.build() as JSON
    }

    /**
     * Fetchs all organisms
     *
     * @return
     */
    def fetch() {
        render biomodelsService.fetchAllOrganisms()
    }

    /**
     * Visualises the data in a plot with D3.js
     */
    def visualise() {
        def organisms = ["children": biomodelsService.build()] as JSON
        render(view: "visualise", model: [organisms: organisms])
    }
}
