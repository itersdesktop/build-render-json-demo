## Introduction

This project aims to give a demonstration of how to build and render JSON in Grails
 application. Read [How to build and render data in JSON format with Grails
](https://www.itersdesktop.com/2020/04/12/how-to-build-and-render-data-in-json-format-with-grails/) to be aware of more details about
 the approach and implementation.
 
## Usage
 
Clone the project in your interest place, let's say `build-render-json-demo` and navigate you inside that place.
 Run `./grailsw` to start grails wrapper for downloading grails framework.
 If there is no error during downloading grails artifacts, you can start the application by running the following
  command from grails prompt. 
  
 ```
grails>run-app
```

Open web browser, navigate the link [http://localhost:8181/build-render-json-demo/biomodels/visualise] and remember to
 replace
 application name, controller and action name so that they are right with your personal customisation.
 
 
## Contact
Author: [Tung Nguyen](mailto:nguyenvungoctung@gmail.com)
