package com.itersdesktop.javatechs.grails

class Organism {
    String name
    String count
    String taxonomy

    String toString() {
        "[Organism] $name ($taxonomy): $count"
    }
}
